const {
	src,
	dest,
	watch,
	parallel,
	series,
} = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const concat = require('gulp-concat');
const uglify = require('gulp-uglify-es').default;
const browserSync = require('browser-sync');
const del = require('del');
const imagemin = require('gulp-imagemin');
const autoprefixer = require('gulp-autoprefixer');

const distPath = 'dist/';
const srcPath = 'src/';

function images() {
	return src('src/images/**/*')
		.pipe(
			imagemin([
				imagemin.mozjpeg({
					quality: 75,
					progressive: true,
				}),
				// imagemin.optipng({ optimizationLevel: 5 }),
				imagemin.svgo({
					plugins: [
						{ removeViewBox: true },
						{ cleanupIDs: true },
					],
				}),
			])
		)
		.pipe(dest(distPath + 'img'));
}

function styles() {
	return src(srcPath + 'scss/styles.scss')
		.pipe(sass({ outputStyle: 'compressed' }))
		.pipe(autoprefixer({ cascade: false }))
		.pipe(concat('styles.min.css'))
		.pipe(dest(distPath + 'css'))
		.pipe(browserSync.stream());
}

function scripts() {
	return src([
		// 'node_modules/jquery/dist/jquery.js',
		srcPath + 'js/*',
	])
		.pipe(concat('app.min.js'))
		.pipe(uglify())
		.pipe(dest(distPath + 'js'))
		.pipe(browserSync.stream());
}

function html() {
	return src([srcPath + 'index.html'])
		.pipe(dest(distPath))
		.pipe(browserSync.stream());
}
function fonts() {
	return src(srcPath + 'fonts/**/*')
		.pipe(dest(distPath + 'fonts/'))
		.pipe(browserSync.stream());
}

function watching() {
	watch([srcPath + 'scss/**/*.scss'], styles);
	watch([srcPath + 'js/*'], scripts);
	watch([srcPath + 'fonts/**/*'], fonts);
	watch([srcPath + '*.html'], html);
}

function reloadPage() {
	browserSync.init({
		server: {
			baseDir: distPath,
			port: 3000,
			keepalive: true,
		},
	});
}

function clean() {
	return del(distPath);
}

exports.clean = clean;

const build = series(
	clean,
	parallel(html, images, scripts, fonts),
	styles
);
const dev = parallel(reloadPage, watching);

exports.build = build;
exports.dev = dev;

exports.default = series(build, dev);
