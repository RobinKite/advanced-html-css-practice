# Nabla

## Description

A small website developed as part of a web design course. Created with HTML, Sass for styling, and automated using Gulp. Completed within approximately 5 hours.

## Links

- [Design](https://www.figma.com/file/AemPAh9sIG0UPiPRqjxmsC/%D0%BA%D0%BE%D0%BD%D1%81%D1%83%D0%BB%D1%8C%D1%82%D0%B0%D1%86%D0%B8%D0%B8-%D0%BF%D0%BE-%D0%B7%D0%B4%D0%BE%D1%80%D0%BE%D0%B2%D1%8C%D1%8E?type=design&node-id=0%3A1&mode=design&t=oZE8K0ZvKowiGB4r-1)
- [Live Demo](https://advanced-html-css-practice-robinkite-304446d82eb86d777e9839d738.gitlab.io/)
